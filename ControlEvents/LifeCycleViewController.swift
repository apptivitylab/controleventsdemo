//
//  LifeCycleViewController.swift
//  ControlEvents
//
//  Created by Jason Khong on 11/5/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class LifeCycleViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var lifecycleLabels: [UILabel]!

    @IBOutlet weak var didLoadLabel: UILabel!
    @IBOutlet weak var willAppearLabel: UILabel!
    @IBOutlet weak var didAppearLabel: UILabel!
    @IBOutlet weak var willDisappearLabel: UILabel!
    @IBOutlet weak var didDisappearLabel: UILabel!
    
    @IBOutlet weak var pushButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        highlightLabel(didLoadLabel)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        highlightLabel(willAppearLabel)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        highlightLabel(didAppearLabel)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        highlightLabel(willDisappearLabel)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        highlightLabel(didDisappearLabel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func highlightLabel(label: UILabel) {
        label.font = UIFont.boldSystemFontOfSize(17)
        if label.textColor == UIColor.redColor() {
            label.textColor = UIColor.greenColor()
        } else {
            label.textColor = UIColor.redColor()
        }
    }

    @IBAction func showViewController(sender: AnyObject) {
        let anotherVC: LifeCycleViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LifeCycleViewController") as! LifeCycleViewController
        self.navigationController?.pushViewController(anotherVC, animated: true)
    }
}
