//
//  ViewController.swift
//  ControlEvents
//
//  Created by Jason Khong on 11/4/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class ControlEventsViewController: UIViewController {

    @IBOutlet weak var mainButton: UIButton!
    
    @IBOutlet var eventLabels: [UILabel]!

    @IBOutlet weak var downLabel: UILabel!
    @IBOutlet weak var downRepeatLabel: UILabel!
    @IBOutlet weak var dragEnterLabel: UILabel!
    @IBOutlet weak var dragExitLabel: UILabel!
    @IBOutlet weak var dragInsideLabel: UILabel!
    @IBOutlet weak var dragOutsideLabel: UILabel!
    @IBOutlet weak var upInsideLabel: UILabel!
    @IBOutlet weak var upOutsideLabel: UILabel!
    @IBOutlet weak var cancelLabel: UILabel!
    
    @IBOutlet var longPressGesture: UILongPressGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetLabels(sender: AnyObject) {
        for label in self.eventLabels {
            label.textColor = UIColor.lightGrayColor()
            label.font = UIFont.systemFontOfSize(17)
        }
    }

    func highlightLabel(label: UILabel) {
        label.textColor = UIColor.redColor()
        label.font = UIFont.boldSystemFontOfSize(17)
    }
    
    @IBAction func touchDown(sender: AnyObject) {
        highlightLabel(self.downLabel)
    }

    @IBAction func touchDownRepeat(sender: AnyObject) {
        highlightLabel(self.downRepeatLabel)
    }

    @IBAction func touchDragEnter(sender: AnyObject) {
        highlightLabel(dragEnterLabel)
    }

    @IBAction func touchDragExit(sender: AnyObject) {
        highlightLabel(dragExitLabel)
    }

    @IBAction func touchDragInside(sender: AnyObject) {
        highlightLabel(dragInsideLabel)
    }

    @IBAction func touchDragOutside(sender: AnyObject) {
        highlightLabel(dragOutsideLabel)
    }
    
    @IBAction func touchUpInside(sender: AnyObject) {
        highlightLabel(upInsideLabel)
    }
    
    @IBAction func touchUpOutside(sender: AnyObject) {
        highlightLabel(upOutsideLabel)
    }

    @IBAction func touchCancel(sender: AnyObject) {
        highlightLabel(cancelLabel)
    }
}

